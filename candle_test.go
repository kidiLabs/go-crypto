package crypto

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestCandle(t *testing.T) {
	c := &Candle{
		O: 1.0,
	}

	assert.Equal(t, 0.25, c.Avg())
}
