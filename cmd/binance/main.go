package main

import (
	"log"

	crypto "gitlab.com/kidiLabs/go-crypto"
)

func main() {
	log.Printf("BuildTime: %v", crypto.BuildTime)
	log.Printf("Commit: %v", crypto.Commit)
	log.Printf("Release: %v", crypto.Release)
}
