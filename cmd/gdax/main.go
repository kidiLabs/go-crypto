package main

import (
	"fmt"
	"log"
	"os"
	"sort"
	"time"

	gdax "github.com/preichenberger/go-gdax"
)

func main() {
	key := os.Getenv("COINBASE_KEY")
	secret := os.Getenv("COINBASE_SECRET")
	passphrase := os.Getenv("COINBASE_PASSPHRASE")

	client := gdax.NewClient(secret, key, passphrase)

	accounts, err := client.GetAccounts()
	if err != nil {
		log.Fatalf("error loading accounts: %s", err)
	}

	for _, a := range accounts {
		fmt.Printf("%s: %v\n", a.Currency, a.Balance)
	}

	currencies, err := client.GetCurrencies()
	if err != nil {
		log.Fatalf("error loading currencies: %s", err)
	}

	for _, c := range currencies {
		fmt.Printf("Id=%s, Name=%s\n", c.Id, c.Name)
	}

	fills := []gdax.Fill{}
	cursor := client.ListFills()
	for cursor.HasMore {
		var page []gdax.Fill
		if err := cursor.NextPage(&page); err != nil {
			log.Fatal(err)
		}

		fills = append(fills, page...)
	}

	sort.Slice(fills, func(i, j int) bool {
		return time.Time(fills[i].CreatedAt).Before(time.Time(fills[j].CreatedAt))
	})

	for _, f := range fills {
		fmt.Printf("Date=%s Product=%s Settle=%v Side=%-4v TradeId=%v Size=%-10v Price=%-8v Fee=%-16v Liquidity=%v FillId=%v\n", time.Time(f.CreatedAt), f.ProductId, f.Settled, f.Side, f.TradeId, f.Size, f.Price, f.Fee, f.Liquidity, f.FillId)
	}

	products, err := client.GetProducts()
	if err != nil {
		log.Fatalf("error loading products: %s", err)
	}

	time.Sleep(5 * time.Second)

	for _, p := range products {
		fmt.Printf("Id=%s, Base=%s, Quote=%s\n", p.Id, p.BaseCurrency, p.QuoteCurrency)

		trades := []gdax.Trade{}
		cursor = client.ListTrades(p.Id)
		for cursor.HasMore {
			var page []gdax.Trade
			if err := cursor.NextPage(&page); err != nil {
				log.Fatal(err)
			}

			trades = append(trades, page...)
		}

		for _, t := range trades {
			fmt.Printf("%v\n", t)
		}
	}
}
