package crypto

import (
	"time"
)

// Candle describe a chart candle
type Candle struct {
	T time.Time
	O float64
	H float64
	L float64
	C float64
	V float64
}

// Avg calculates the average price
func (c Candle) Avg() float64 {
	return (c.O + c.H + c.L + c.C) / 4
}
